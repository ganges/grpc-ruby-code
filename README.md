# Ganges Grpc Ruby Code

This gem contains the ruby code auto-generated from https://gitlab.com/ganges/grpc-proto-definitions

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'grpc-ruby-code'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install grpc-ruby-code

## Usage

Require the files as needed

## Development

Make sure that the grpc-proto-definitions repository is in the same parent folder then run `rake grpc:generate` to generate the ruby files based on the current grpc proto definitions.

To experiment with that code, run `bin/console` for an interactive prompt.

Once you are satisfied commit the changes and push them, updating the version number in the gemspec as required.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
